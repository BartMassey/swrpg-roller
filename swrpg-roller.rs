// Copyright © 2018 Bart Massey

// Derived from code Copyright (c) 2018 Mark Bahnman
// and used under license.

// This work is made available under the "MIT
// License". Please see the file LICENSE in this
// distribution for license terms.

//! Star Wars RPG dice roller.

extern crate rand;

use rand::Rng;

use std::env;

/// Kinds of die symbol.
#[derive(Debug, Clone, Copy)]
enum Symbol {
    Advantage,
    Threat,
    Success,
    Failure,
    Triumph,
    Despair,
}
use Symbol::*;

/// Die as a list of faces with counts.Each face is a list
/// of symbols.  Each die is defined globally, so static
/// lifetimes.
struct Die(&'static [&'static [Symbol]]);

/// Description format for a die.
struct DieDesc {
    name: &'static str,
    die: Die,
}

/// The dice.
const DICE: &[DieDesc] = &[
    DieDesc{name: "boost", die: Die(&[
        &[],
        &[],
        &[Success],
        &[Advantage],
        &[Advantage, Success],
        &[Advantage, Advantage],
    ])},
    DieDesc{name: "setback", die: Die(&[
        &[],
        &[],
        &[Threat],
        &[Threat],
        &[Failure],
        &[Failure],
    ])},
    DieDesc{name: "proficiency", die: Die(&[
        &[],
        &[Success, Success],
        &[Success, Success],
        &[Advantage, Advantage],
        &[Advantage, Advantage],
        &[Success],
        &[Success],
        &[Advantage],
        &[Advantage, Success],
        &[Advantage, Success],
        &[Advantage, Success],
        &[Triumph],
    ])},
    DieDesc{name: "challenge", die: Die(&[
        &[],
        &[Failure, Failure],
        &[Failure, Failure],
        &[Threat, Threat],
        &[Threat, Threat],
        &[Failure],
        &[Failure],
        &[Threat],
        &[Threat, Failure],
        &[Threat, Failure],
        &[Threat, Failure],
        &[Despair],
    ])},
    DieDesc{name: "ability", die: Die(&[
        &[],
        &[Success],
        &[Success],
        &[Advantage],
        &[Advantage],
        &[Advantage, Advantage],
        &[Advantage, Success],
        &[Success, Success],
    ])},
    DieDesc{name: "difficulty", die: Die(&[
        &[Failure],
        &[Threat],
        &[Threat],
        &[Threat],
        &[Threat, Threat],
        &[Threat, Failure],
        &[Failure, Failure],
    ])},
];

/// Find a die from its description. This function is
/// currently linear-time in the number of descriptions:
/// this could easily be sped up with a `HashMap` if it
/// mattered.
fn find_die(desc: &str) -> Result<&'static DieDesc, ()> {
    for d in DICE.iter() {
        if d.name.starts_with(desc) {
            return Ok(d);
        }
    }
    Err(())
}

/// Handle an error by printing a message and exiting with
/// failure.
macro_rules! report_error {
    ($($args:expr),+) => {{
        eprintln!($($args),*);
        std::process::exit(1);
    }}
}

/// `try!` with `handle_error!`.
macro_rules! unwrap_error {
    ($result:expr, $($args:expr),+) => {
        match $result {
            Ok(r) => r,
            Err(_) => report_error!($($args),*),
        }
    }
}

/// Process the roll descriptions and produce a report.
fn main() {
    // Count of rolled faces.
    let mut totals: [u64; Despair as usize + 1] = [0; Despair as usize + 1];

    // Add the value of a symbol to the totals. `Triumph`
    // and `Despair` are speshul. Kipling would be proud.
    let bump_totals = |s, totals: &mut [u64]| {
        totals[s as usize] += 1;
        match s {
            Triumph => totals[Success as usize] += 1,
            Despair => totals[Failure as usize] += 1,
            _ => ()
        }
    };

    // Make and record rolls.
    let mut prng = rand::thread_rng();
    for spec in env::args().skip(1) {
        let fields: Vec<&str> = spec.split(":").collect();
        let die = unwrap_error!(find_die(fields[0]), "bad die desc {}", fields[0]);
        let count: u64 = match fields.len() {
            1 => 1,
            2 => unwrap_error!(fields[1].parse(), "bad die count {}", fields[1]),
            _ => report_error!("bad roll desc {}", spec),
        };
        for _ in 0..count {
            let roll = prng.choose(die.die.0).unwrap();
            for face in *roll {
                bump_totals(*face, &mut totals);
            }
        }
    }

    // Find the total at a given face.
    let lookup = |face| totals[face as usize];

    // Print the result.

    // Success / failure.
    let msg = match (lookup(Success), lookup(Failure)) {
        (s, f) if s > f => format!("success with {} successes", s - f),
        (s, f) if s < f => format!("failure with {} failures", f - s),
        _ => "failure with no net successes or failures".to_string(),
    };
    print!("Roll was a {}, ", msg);

    // Threat / advantage.
    let msg = match (lookup(Threat), lookup(Advantage)) {
        (t, a) if t > a => format!("{} threat", t - a),
        (t, a) if t < a => format!("{} advantage", a - t),
        _ => "no threat or advantage".to_string(),
    };
    print!("{}", msg);

    // Triumph / despair.
    let msg = match (lookup(Triumph), lookup(Despair)) {
        (t, d) if t > 0 && d > 0 => format!(", {} triumph and {} despair.", t, d),
        (t, _) if t > 0 => format!(" and {} triumph.", t),
        (_, d) if d > 0 => format!(" and {} despair.", d),
        _ => ".".to_string(),
    };
    println!("{}", msg);
}
