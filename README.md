# Star Wars RPG Die Roller
Copyright (c) 2018 Bart Massey  
Derived from code Copyright (c) 2018 Mark Bahnman and used
under license.

This program apparently rolls dice for the Star Wars
RPG. I've never played this game, but was inspired to create
it as a tutorial response to Mark Bahnman's
[Reddit post](https://www.reddit.com/r/rust/comments/98r1ek/questions_after_writing_my_first_rust_app/).
The code is not super-efficient, but is hopefully reasonably
clean. It should produce roughly the same output as
Bahnman's program.

To use, provide a list of dice names on the command
line. The dice name may be any prefix of the full name.  An
optional `:`*count* may be appended to the name to get
multiples. Example:

```
$ cargo run p:5 c
Roll was a success with 5 successes, 1 advantage and 1 despair.
```

This work is made available under the "MIT License". Please
see the file `LICENSE` in this distribution for license terms.

